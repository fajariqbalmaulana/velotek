<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function master_barang() {
        return $this->hasMany(MasterBarang::class);
    }
}
