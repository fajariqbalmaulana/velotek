<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPembelianBarang extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'transaksi_pembelian_barang';

    public function pembelian() {
        return $this->belongsTo(TransaksiPembelian::class);
    }

    public function barang() {
        return $this->belongsTo(MasterBarang::class);
    }
}
