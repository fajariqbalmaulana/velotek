<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterBarang extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'master_barang';

    public function category() {
        return $this->belongsTo(ItemCategory::class, 'item_category_id');
    }

    public function cart() {
        return $this->hasOne(Cart::class);
    }

    public function transactions() {
        return $this->hasManyThrough(Transaction::class, TransactionDetail::class);
    }
}
