<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPembelian extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'transaksi_pembelian';

    public function pembelianBarang() {
        return $this->hasOne(TransaksiPembelianBarang::class);
    }
}
