<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $master_barangs = MasterBarang::doesntHave('cart')->where('stock', '>', 0)->get()->sortBy('name');
        $master_barangCarts = MasterBarang::has('cart')->get()->sortByDesc('cart.created_at');

        return view('home', compact(['master_barangs', 'master_barangCarts']));
    }
}
