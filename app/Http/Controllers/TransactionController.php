<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function store()
    {
        DB::beginTransaction();

        try {
            auth()->user()
                ->transactions()
                ->create(request()->all())
                ->details()
                ->createMany(Cart::all()->map(function ($cart) {
                    return [
                        'master_barang_id'  => $cart->master_barang_id,
                        'quantity'          => $cart->quantity,
                        'subtotal'          => $cart->master_barang->harga_satuan * $cart->quantity
                    ];
                })->toArray());

            DB::table('carts')->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('transaction.show', Transaction::latest()->first());
    }

    public function index()
    {
        $transactions = Transaction::latest()->get();

        return view('transaction.index', compact('transactions'));
    }

    public function show(Transaction $transaction)
    {
        return view('transaction.show', compact('transaction'));
    }
}
