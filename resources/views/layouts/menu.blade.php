<li class="nav-item">
    <a href="/dashboard" class="nav-link {{ Route::current()->getName() == 'dashboard' ? 'active' : '' }}">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
            Dashboard
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-archive"></i>
        <p>
            Master
            <i class="right fas fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="./index.html" class="nav-link">
                <i class="fas fa-cubes nav-icon"></i>
                <p>Master Barang</p>
            </a>
        </li>
        {{-- <li class="nav-item">
            <a href="./index2.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Dashboard v2</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="./index3.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Dashboard v3</p>
            </a>
        </li> --}}
    </ul>
</li>
<li class="nav-item">
    <a href="/transaksi" class="nav-link {{ Route::current()->getName() == 'transaksi' ? 'active' : '' }}">
        <i class="nav-icon fas fa-shopping-cart"></i>
        <p>
            Transaksi
        </p>
    </a>
</li>
