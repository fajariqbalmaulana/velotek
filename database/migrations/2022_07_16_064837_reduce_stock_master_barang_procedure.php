<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ReduceStockMasterBarangProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE OR REPLACE PROCEDURE reduce_stock(transaction_detail_id INT)
            BEGIN
                UPDATE master_barang
                SET stock=stock - master_barang_quantity(transaction_detail_id)
                WHERE id=master_barang_id(transaction_detail_id);
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
