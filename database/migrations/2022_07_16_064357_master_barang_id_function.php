<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MasterBarangIdFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE OR REPLACE FUNCTION master_barang_id(transaction_detail_id INT) RETURNS INT
            BEGIN
                DECLARE id_master_barang INT;
                SET id_master_barang = (SELECT master_barang_id FROM transaction_details WHERE id=transaction_detail_id);
                RETURN id_master_barang;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
