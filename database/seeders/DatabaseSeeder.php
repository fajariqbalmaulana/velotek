<?php

namespace Database\Seeders;

use App\Models\ItemCategory;
use App\Models\MasterBarang;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'name'              => 'Super Admin',
            'username'          => 'superadmin',
            'email'             => 'admin@example.com',
            'email_verified_at' => now(),
            'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ]);
        User::factory(29)->create();

        MasterBarang::create([
            'nama_barang' => 'Sabun Batang',
            'item_category_id' => '4',
            'harga_satuan' => '3000',
            'stock' => '300',
            'description' => 'GIV',
        ]);
        MasterBarang::create([
            'nama_barang' => 'Mie Instan',
            'item_category_id' => '1',
            'harga_satuan' => '2000',
            'stock' => '300',
            'description' => 'Indomie Ayam Bawang',
        ]);
        MasterBarang::create([
            'nama_barang' => 'Pensil',
            'item_category_id' => '3',
            'harga_satuan' => '1000',
            'stock' => '300',
            'description' => 'Faber Castell 2B',
        ]);
        MasterBarang::create([
            'nama_barang' => 'Kopi Sachet',
            'item_category_id' => '2',
            'harga_satuan' => '1500',
            'stock' => '300',
            'description' => 'Good Day Mochacinno',
        ]);
        MasterBarang::create([
            'nama_barang' => 'Air Minum Galon',
            'item_category_id' => '2',
            'harga_satuan' => '20000',
            'stock' => '300',
            'description' => 'Aqua',
        ]);

        ItemCategory::create([
            'name' => 'Makanan',
        ]);
        ItemCategory::create([
            'name' => 'Minuman',
        ]);
        ItemCategory::create([
            'name' => 'Alat Tulis',
        ]);
        ItemCategory::create([
            'name' => 'Perlengkapan Rumah Tangga',
        ]);
    }
}
